#-----Location of Executables Used by MAKER/EVALUATOR
makeblastdb=/home/linuxbrew/.linuxbrew/bin/makeblastdb #location of NCBI+ makeblastdb executable
blastn=/home/linuxbrew/.linuxbrew/bin/blastn #location of NCBI+ blastn executable
blastx=/home/linuxbrew/.linuxbrew/bin/blastx #location of NCBI+ blastx executable
tblastx=/home/linuxbrew/.linuxbrew/bin/tblastx #location of NCBI+ tblastx executable
formatdb= #location of NCBI formatdb executable
blastall= #location of NCBI blastall executable
xdformat= #location of WUBLAST xdformat executable
blasta= #location of WUBLAST blasta executable
RepeatMasker=/home/linuxbrew/.linuxbrew/bin/RepeatMasker #location of RepeatMasker executable
exonerate=/home/linuxbrew/.linuxbrew/bin/exonerate #location of exonerate executable

#-----Ab-initio Gene Prediction Algorithms
snap=/home/linuxbrew/.linuxbrew/bin/snap #location of snap executable
gmhmme3= #location of eukaryotic genemark executable
gmhmmp= #location of prokaryotic genemark executable
augustus= #location of augustus executable
fgenesh= #location of fgenesh executable
tRNAscan-SE= #location of trnascan executable
snoscan= #location of snoscan executable

#-----Other Algorithms
probuild= #location of probuild executable (required for genemark)
