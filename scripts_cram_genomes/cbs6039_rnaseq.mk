# /nfs/gems_sata/heitman/camy/camy_raw_data/CBS6039_rnaseq
# ASU_APOSW_5_1_D0YLDACXX.IND14.fastq.gz
# ASU_APOSW_5_2_D0YLDACXX.IND14.fastq.gz



FASTQC=/home/josh/collabs/software/FastQC/fastqc
##------------------------------------------------------------
## DIRS
##------------------------------------------------------------
RAW_FASTQ_DIR := /nfs/gems_sata/heitman/camy/camy_raw_data/CBS6039_rnaseq
WORK_DIR := workspace
QC_DIR := $(WORK_DIR)/qc
TEMP_DIR := $(WORK_DIR)/temp
FINAL_FASTQ_DIR := $(WORK_DIR)/fastqs
GENOME_DIR := $(WORK_DIR)/genome
# BWA_OUTDIR := $(WORK_DIR)/bams
INFO_DIR := info

TOPHAT_BASE_DIR := $(WORK_DIR)/thout
##------------------------------------------------------------
RAW_FASTQS := $(wildcard ${RAW_FASTQ_DIR}/*.fastq.gz)
FASTQS_BASE = $(notdir $(subst .fastq.gz,,$(RAW_FASTQS)))
FASTQC_FILES = $(addprefix $(QC_DIR)/,$(addsuffix _fastqc.zip,$(FASTQS_BASE)))
FASTQ_SUFFIX = D0YLDACXX.IND14.fastq.gz
FASTQ_PREFIX = ASU_APOSW_5
ADAPTER_FASTA = $(INFO_DIR)/illumina_adapters.fasta
TRIMMED_FASTQS =  $(addprefix $(FINAL_FASTQ_DIR)/,$(addsuffix .trim.fastq.gz,$(FASTQS_BASE)))

# #--------------------------------------------------
# RAW_FASTQS_FULLPATH := $(wildcard $(RAW_FASTQ_DIR)/*.fastq.gz)
# FASTQS := $(notdir $(RAW_FASTQS_FULLPATH))
# FINAL_FASTQS := $(addprefix $(FINAL_FASTQ_DIR)/,$(FASTQS))
# #--------------------------------------------------
BAM_FILE := $(TOPHAT_BASE_DIR)/$(FASTQ_PREFIX)/accepted_hits.bam

# FINAL_FASTQS = $(addprefix $(FINAL_FASTQ_DIR)/$(FASTQ_PREFIX),.un1.fastq.gz .un2.fastq.gz .join.fastq.gz)
# FINAL_BAMS = $(addprefix $(BWA_OUTDIR)/$(FASTQ_PREFIX),.join.bam .pair.bam)
# MERGED_BAM := $(BWA_OUTDIR)/$(FASTQ_PREFIX).merge.bam
# MERGED_BAI := $(MERGED_BAM).bai
# MERGED_BIGWIG := $(MERGED_BAM:.bam=.bigWig)
# MERGED_COVERAGE_PLOT := $(MERGED_BAM:.bam=.coverage.pdf)
# MERGED_COVERAGE_CENPA_PLOT := $(MERGED_BAM:.bam=.coverage_cenpa.pdf)

# SOURCE_GENOME := $(CAMY)/cram_genomes/revised_genomes/cbs6039_genome_v3.fasta
# GENOME = $(GENOME_DIR)/$(notdir $(SOURCE_GENOME:.fasta=.fa))

# INDEXBASE=$(basename $(GENOME))
# INDEX_FILES=$(addprefix $(INDEXBASE),.bwt .pac .ann .amb .sa)

.PRECIOUS: $(INDEX_FILES)

.SECONDARY: $(INDEX_FILES)

##------------------------------------------------------------
NUMTHREADS := 16
MAXMEM := 10000000000
##------------------------------------------------------------
dir_guard=@mkdir -p $(@D)
##------------------------------------------------------------
all : $(FINAL_FASTQS) $(MERGED_BAM) $(MERGED_BIGWIG) $(MERGED_COVERAGE_PLOT)

trim : $(TRIMMED_FASTQS)

run_qc : $(FASTQC_FILES)

run_bam : $(BAM_FILE)

test : $(GTF) $(BT2_INDEX_FILES)
	echo $(ADAPTER_FASTA)
	echo $(TRIMMED_FASTQS)
	echo "RAW_FASTQ_DIR:" $(RAW_FASTQ_DIR)
	echo "FASTQ_SUFFIX:" $(FASTQ_SUFFIX)
	ls $(RAW_FASTQ_DIR)/*_1_$(FASTQ_SUFFIX)
	ls $(RAW_FASTQ_DIR)/*_2_$(FASTQ_SUFFIX)

##------------------------------------------------------------
#--------------------------------------------------
GENOME_DIR=genome
INDEXBASE=$(GENOME_DIR)/CBS6039_V3
SEQ_URL="ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/720/205/GCF_001720205.1_Cryp_amyl_CBS6039_V3/GCF_001720205.1_Cryp_amyl_CBS6039_V3_genomic.fna.gz"
GTF_URL="ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/720/205/GCF_001720205.1_Cryp_amyl_CBS6039_V3/GCF_001720205.1_Cryp_amyl_CBS6039_V3_genomic.gff.gz"


SEQ= $(INDEXBASE).fa
GTF= $(INDEXBASE).gtf
# INDEXBASE=$(basename $(GENOME))
BT2_INDEX_FILES=$(addprefix $(INDEXBASE),.1.bt2 .2.bt2 .3.bt2 .4.bt2 .rev.1.bt2 .rev.2.bt2)
#--------------------------------------------------
#===============================================================================
#===============================================================================
# Download and merge reference genomes 
refgenome : $(SEQ) $(GTF) $(BT2_INDEX_FILES)

$(SEQ) :
	$(dir_guard)
	# The following changes the chromosome names in the genome sequence file so they are compatible with the gtf file
	curl $(SEQ_URL) | zcat > $@.tmp
	mv $@.tmp $@

$(GTF) :
	$(dir_guard)
	curl $(GTF_URL) | zcat > $@.tmp
	mv $@.tmp $@
#--------------------------------------------------------------------------------
# Index reference genome
# $(BT2_INDEX_FILES) : $(SEQ)
# 	bowtie2-build $< $(INDEXBASE)
%.1.bt2 %.2.bt2 %.3.bt2 %.4.bt2 %.rev.1.bt2 %.rev.2.bt2 : %.fa
	bowtie2-build $< $*

%.bwt %.pac %.ann %.amb %.sa : %.fa
	bwa index $< -p $*
#--------------------------------------------------------------------------------

##------------------------------------------------------------
##------------------------------------------------------------
${QC_DIR}/%_fastqc.zip : %.fastq.gz
	$(dir_guard)
	${FASTQC} --outdir $(@D) --threads ${NUMTHREADS} --noextract $^
##------------------------------------------------------------
$(FINAL_FASTQ_DIR)/%1_D0YLDACXX.IND14.trim.fastq.gz $(FINAL_FASTQ_DIR)/%2_D0YLDACXX.IND14.trim.fastq.gz : $(ADAPTER_FASTA) $(RAW_FASTQ_DIR)/%1_D0YLDACXX.IND14.fastq.gz $(RAW_FASTQ_DIR)/%2_D0YLDACXX.IND14.fastq.gz
	$(dir_guard)
	@echo $(@D)/$*_R1.tmp.gz
	@echo $(@D)/$*_R2.tmp.gz
	@echo $@
	fastq-mcf $(word 1,$^) $(word 2,$^) $(word 3,$^) -o $(@D)/$*1_D0YLDACXX.IND14.trim.tmp.gz -o $(@D)/$*2_D0YLDACXX.IND14.trim.tmp.gz
	mv $(@D)/$*1_D0YLDACXX.IND14.trim.tmp.gz $(@D)/$*1_D0YLDACXX.IND14.trim.fastq.gz
	mv $(@D)/$*2_D0YLDACXX.IND14.trim.tmp.gz $(@D)/$*2_D0YLDACXX.IND14.trim.fastq.gz

##================================================================================
##================================================================================
BLAST_DB_FILES = $(addprefix $(basename $(GENOME)),.db.nhr .db.nin .db.nsq)
BLAST_DIR = blastout

CENPA_CHIP_SEQS_ORIGINAL := ${CAMY}/other_sequences/cenp_chip_seqs.fna
CENPA_CHIP_SEQS = $(INFO_DIR)/cenpa_chip.fasta
CENPA_CHIP_GFF = $(BLAST_DIR)/cenpa_chip_blast.gff

$(CENPA_CHIP_SEQS) : $(CENPA_CHIP_SEQS_ORIGINAL)
	$(dir_guard)
	ln -s $< $@

runblast : $(BLAST_DB_FILES) $(CENPA_CHIP_GFF) $(MERGED_COVERAGE_CENPA_PLOT)


%.db.nhr %.db.nin %.db.nsq : %.fa
	$(dir_guard)
	makeblastdb -in $< -dbtype nucl -out $*.db

$(BLAST_DIR)/%_blast.xml : $(INFO_DIR)/%.fasta $(BLAST_DB_FILES)
	$(dir_guard)
	blastn -outfmt 5 -num_threads $(NUMTHREADS) -query $< -db $(basename $(word 2,$^)) -out $@.tmp
	mv $@.tmp $@

%.gff : %.xml
	$(dir_guard)
	# python2.7 $SCRIPTS/parse_blast_xml.py $CAMY/local_blast_dbs/blastout/cenp_chip_on_genofinal_pbjelly.xml --gff $CAMY/local_blast_dbs/blastout/cenp_chip_on_genofinal_pbjelly.gff
	python2.7 ${SCRIPTS}/parse_blast_xml.py $< --gff $@

# python2.7 $SCRIPTS/parse_blast_xml.py $CAMY/local_blast_dbs/blastout/cenp_chip_on_genofinal_pbjelly.xml --gff $CAMY/local_blast_dbs/blastout/cenp_chip_on_genofinal_pbjelly.gff
#--------------------------------------------------------------------------------
%.coverage_cenpa.pdf : $(GENOME) %.bigWig $(BLAST_DIR)/cenpa_chip_blast.gff
	$(dir_guard)
	# python2.7 $(SCRIPTS)/genome_plot.py $(word 1,$^) --sortplot --wig $(word 2,$^) dummycolor --gff $(word 3,$^) black -o $@
	python2.7 $(SCRIPTS)/genome_plot.py $(word 1,$^) --sortplot --gff $(word 3,$^) black -o $@
	mv $@.tmp $@
	# need to have multple y-axes?
	# http://stackoverflow.com/questions/5484922/secondary-axis-with-twinx-how-to-add-to-legend
	# http://matplotlib.org/examples/axes_grid/demo_parasite_axes2.html
	# http://stackoverflow.com/questions/9103166/multiple-axis-in-matplotlib-with-different-scales




#--------------------------------------------------------------------------------
# Run Tophat
#-----------
# -T/--transcriptome-only 	Only align the reads to the transcriptome and report only those mappings as genomic mappings.
# -x/--transcriptome-max-hits 	Maximum number of mappings allowed for a read, when aligned to the transcriptome (any reads found with more then this number of mappings will be discarded).
# -M/--prefilter-multihits 	When mapping reads on the transcriptome, some repetitive or low complexity reads that would be discarded in the context of the genome may appear to align to the transcript sequences and thus may end up reported as mapped to those genes only. This option directs TopHat to first align the reads to the whole genome in order to determine and exclude such multi-mapped reads (according to the value of the -g/--max-multihits option). 
# -G known_genes.gtf \
# --transcriptome-index=transcriptome_data/known \
#--------------------------------------------------
$(TOPHAT_BASE_DIR)/%/accepted_hits.bam : $(GTF) $(FINAL_FASTQ_DIR)/%_1_D0YLDACXX.IND14.trim.fastq.gz $(FINAL_FASTQ_DIR)/%_2_D0YLDACXX.IND14.trim.fastq.gz $(BT2_INDEX_FILES)
	$(eval OUTDIR :=  $(@D)_tmpthoutdir)
	mkdir -p $(OUTDIR)
	tophat --output-dir $(OUTDIR) --GTF $(word 1,$^) \
		--transcriptome-max-hits 1 --max-multihits 1 \
		--max-intron-length 4000 --num-threads $(NUMTHREADS) \
		--library-type fr-unstranded --no-coverage-search \
		$(INDEXBASE) $(word 2,$^) $(word 3,$^)
	mv $(OUTDIR) $(@D)

$(TOPHAT_BASE_DIR)/%.bam : $(TOPHAT_BASE_DIR)/%/accepted_hits.bam
	ln -s $< $@

$(TOPHAT_BASE_DIR)/%/test.bam : $(RAW_FASTQ_DIR)/%$(FASTQ_SUFFIX) $(BT2_INDEX_FILES)
	$(eval OUTDIR :=  $(@D)_tmpthoutdir)
	mkdir -p $(OUTDIR)
	echo "blah" > $(OUTDIR)/$(@F)
	mv $(OUTDIR) $(@D)
#--------------------------------------------------------------------------------
# Index BAMs
%.bam.bai : %.bam
	$(SAMTOOLS) index $^
