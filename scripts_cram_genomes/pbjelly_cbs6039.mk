# make all --load-average 8 --jobs --warn-undefined-variables # for parallel make
# make all TRUNCATE="400"  -l 8 -j 
# /ssh:microbe:/home/josh/collabs/HeitmanLab/camy_centromere/cram_genomes/scripts/pbjelly_cbs6039.mk

MUMMER_DIR=$(COLLAB)/software/MUMmer3.23
## MUMMER_DIR=/usr/local/bin

DATA_DIR := data
OUT_DIR := out
RAW_DIR := /nfs/gems_sata/heitman/camy/camy_raw_data/pacbio
REF_DIR := $(CAMY)/cram_genomes/genoscope_genomes
MAPPING_DIR := $(OUT_DIR)/mapping
SUPPORT_DIR := $(OUT_DIR)/support

HONEY_DIR := honey_out

REF_FILE:= cbs6039_genome_v2.fasta
ORIG_REF := $(REF_DIR)/$(REF_FILE)
REF := $(DATA_DIR)/$(REF_FILE)
REF_INDEX=$(addsuffix .sa,$(REF))

PACBIO_1 := $(DATA_DIR)/cbs6039_duke_pacbio.fastq
PACBIO_2 := $(DATA_DIR)/cbs6039_unc_pacbio.fastq
# PACBIO_1_GZ := $(RAW_DIR)/pacbio_duke/filtered_subreads_SS1.fastq.gz
# PACBIO_2_GZ := $(RAW_DIR)/pacbio_unc/022_SS001_10Kb_B01_0141_4_filtered_subreads.fastq.gz
PACBIO_1_GZ := $(RAW_DIR)/cbs6039_duke_pacbio.fastq.gz
PACBIO_2_GZ := $(RAW_DIR)/cbs6039_unc_pacbio.fastq.gz

PROTOCOL := $(CAMY)/cram_genomes/config/pbjelly_cbs6039_makefile.xml

# PB_FASTQS = $(PACBIO_2) # $(PACBIO_1)
PB_FASTQS = $(PACBIO_2) $(PACBIO_1)
MAPPING_FILES := $(addprefix $(MAPPING_DIR),$(addsuffix .m4,$(notdir $(PB_FASTQS))))
SUPPORT_FILES := $(addprefix $(SUPPORT_DIR),$(addsuffix .gml,$(notdir $(PB_FASTQS))))
GAP_FILLED_FASTA := $(OUT_DIR)/jelly.out.fasta

MUMMER_PREFIX:= cbs6039_genome_v2_to_v3
CBS6039_V3:= cbs6039_genome_v3.fasta

THREADS=10

dir_guard=@mkdir -p $(@D)

BLASR_PATH=/opt/blasr/alignment/bin/
PB_SETUP=/home/josh/collabs/software/PBSuite_14.1.15/setup.sh
setpath=@export PATH=$(PATH):$(BLASR_PATH); source $(PB_SETUP)

# .ONESHELL:
.PRECIOUS :
.SECONDARY : 

all : $(GAP_FILLED_FASTA) $(CBS6039_V3) todo

todo :
	@echo "Nothing TODO right now"

$(REF) : $(ORIG_REF)
	$(dir_guard)
	ln -s $< $@

# TRUNCATE=
$(DATA_DIR)/%.fastq : $(RAW_DIR)/%.fastq.gz
ifdef TRUNCATE
	$(dir_guard)
	@echo "Truncating FASTQs to $(TRUNCATE) lines"
	gzip -dc < $< | head -n $(TRUNCATE) > $@
else
	$(dir_guard)
	@echo "Using full length FASTQs"
	gzip -dc < $< > $@
endif


# $(PACBIO_1) : $(PACBIO_1_GZ)
# 	$(dir_guard)
# 	ifdef $(TRUNCATE)
# 	gzip -dc < $< | head -n $(TRUNCATE) > $@
# 	else
# 	gzip -dc < $< > $@
# 	endif

# $(PACBIO_2) : $(PACBIO_2_GZ)
# 	$(dir_guard)
# 	# gzip -dc < $< > $@
# 	gzip -dc < $< | head -n 400 > $@

setup : $(REF_INDEX)

%.fasta.sa : $(PROTOCOL) %.fasta
	$(setpath) ; Jelly.py setup $<

# mapping : $(PROTOCOL) $(PACBIO_2) $(PACBIO_1) $(REF_INDEX)
# 	mkdir -p $(OUT_DIR)
# 	$(setpath) ; Jelly.py mapping $<

mapping : $(MAPPING_FILES)

# $(MAPPING_FILES) : $(PROTOCOL) $(REF_INDEX) $(PB_FASTQS) 
# 	$(dir_guard)
# 	$(setpath) ; Jelly.py mapping $<

#--------------------------------------------------
$(MAPPING_FILES) : $(MAPPING_DIR)/mapping.stamp

.INTERMEDIATE: $(MAPPING_DIR)/mapping.stamp
$(MAPPING_DIR)/mapping.stamp : $(PROTOCOL) $(REF_INDEX) $(PB_FASTQS)
	$(dir_guard)
	@rm -f $@.tmp
	@touch $@.tmp
	$(setpath) ; Jelly.py mapping $<
	@mv -f $@.tmp $@

support : $(SUPPORT_FILES)

# support : $(PROTOCOL) $(REF_INDEX) $(MAPPING_FILES)
# 	mkdir -p $(OUT_DIR)
# 	$(setpath) ; Jelly.py support $<


$(SUPPORT_FILES) : $(SUPPORT_DIR)/support.stamp

.INTERMEDIATE: $(SUPPORT_DIR)/support.stamp
$(SUPPORT_DIR)/support.stamp : $(PROTOCOL) $(REF_INDEX) $(MAPPING_FILES)
	$(dir_guard)
	@rm -f $@.tmp
	@touch $@.tmp
	$(setpath) ; Jelly.py support $<
	@mv -f $@.tmp $@

#--------------------------------------------------
EXTRACTION_FILE = $(OUT_DIR)/assembly/masterSupport.bml
extraction : $(EXTRACTION_FILE)

$(EXTRACTION_FILE) : $(PROTOCOL) $(REF_INDEX) $(SUPPORT_FILES)
	$(dir_guard)
	$(setpath) ; Jelly.py extraction $<

# PB_FINAL_FILES := $(addprefix $(OUT_DIR)/, gap_fill_status.txt  jelly.out.fasta  jelly.out.qual  liftOverTable.json)
#--------------------------------------------------
$(GAP_FILLED_FASTA) : $(PROTOCOL) $(REF_INDEX) $(EXTRACTION_FILE)
	# $(dir_guard)
	$(setpath) ; Jelly.py assembly $<
	$(setpath) ; Jelly.py output $<
#--------------------------------------------------
##======================================================================
##======================================================================
%.btab : %.delta
	$(MUMMER_DIR)/show-coords -B -r $< > $@.tmp
	mv $@.tmp  $@

$(MUMMER_PREFIX).delta : $(ORIG_REF) $(GAP_FILLED_FASTA)
	$(MUMMER_DIR)/nucmer --maxgap=500 --mincluster=100 --prefix=$(basename $@)_TMP $(word 1,$^) $(word 2,$^)
	mv $(basename $@)_TMP.delta $@

$(CBS6039_V3) : $(MUMMER_PREFIX).btab $(GAP_FILLED_FASTA)
	## python2.7 /Users/josh/Documents/BioinfCollabs/scripts/match_sequences.py v2_to_v3.btab --rename cbs6039_genome_v3.fasta --out /tmp/test.fasta
	python2.7 $(SCRIPTS)/match_sequences.py $(word 1,$^) --rename $(word 2,$^) --out $@.tmp
	mv $@.tmp $@
##======================================================================
##======================================================================
# seqretsplit cbs6039_genome_v2.fasta -outseq seqoutall
##======================================================================
##======================================================================
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## PBHoney

HONEY_SAMFILES := $(addprefix $(HONEY_DIR)/,$(addsuffix .sam,$(notdir $(basename $(PB_FASTQS)))))
HONEY_BAMFILES := $(addprefix $(HONEY_DIR)/,$(addsuffix .bam,$(notdir $(basename $(PB_FASTQS)))))
# HONEY_TAIL_BAMS := $(addprefix $(HONEY_DIR)/,$(addsuffix .tails.bam,$(notdir $(basename $(PB_FASTQS)))))
# HONEY_FINAL_BAMS := $(addprefix $(HONEY_DIR)/,$(addsuffix .final.bam,$(notdir $(basename $(PB_FASTQS)))))
# HONEY_FINAL_BAIS := $(addsuffix .bai, $(HONEY_FINAL_BAMS))
# HONEY_TAILS := $(addprefix $(HONEY_DIR)/,$(addsuffix .final.hon.tails,$(notdir $(basename $(PB_FASTQS)))))
# HONEY_SPOTS := $(addprefix $(HONEY_DIR)/,$(addsuffix .final.hon.spots,$(notdir $(basename $(PB_FASTQS)))))
#----------------------------------------
MERGED_BAM=$(HONEY_DIR)/cbs6039_pacbio_merge.bam
HONEY_TAIL_BAMS := $(addprefix $(HONEY_DIR)/,$(addsuffix .tails.bam,$(notdir $(basename $(MERGED_BAM)))))
HONEY_FINAL_BAMS := $(addprefix $(HONEY_DIR)/,$(addsuffix .final.bam,$(notdir $(basename $(MERGED_BAM)))))
HONEY_FINAL_BAIS := $(addsuffix .bai, $(HONEY_FINAL_BAMS))
HONEY_TAILS := $(addprefix $(HONEY_DIR)/,$(addsuffix .final.hon.tails,$(notdir $(basename $(MERGED_BAM)))))
HONEY_SPOTS := $(addprefix $(HONEY_DIR)/,$(addsuffix .final.hon.spots,$(notdir $(basename $(MERGED_BAM)))))


honey : $(HONEY_BAMFILES) $(HONEY_TAIL_BAMS) $(HONEY_FINAL_BAMS) $(HONEY_FINAL_BAIS) $(HONEY_TAILS) $(HONEY_SPOTS)  # $(HONEY_SAMFILES) $(HONEY_BAMFILES)
	echo $(HONEY_SAMFILES)
	echo $(HONEY_BAMFILES)

$(HONEY_DIR)/%.sam : $(DATA_DIR)/%.fastq $(CBS6039_V3)
	$(dir_guard)
	$(setpath) ; blasr $(word 1,$^) $(word 2,$^) -bestn 1 -nCandidates 15 -sdpTupleSize 6 -minPctIdentity 75 -affineAlign -noSplitSubreads -nproc $(THREADS) -sam -clipping soft -out $@.tmp
	mv $@.tmp $@

$(MERGED_BAM) : $(HONEY_BAMFILES)
	samtools merge $@.tmp $^
	mv $@.tmp $@

# blasr $inputReads $reference -bestn 1 -nCandidates 15 -sdpTupleSize 6 -minPctIdentity 75 -affineAlign -noSplitSubreads -nproc 1 -sam -clipping soft -out mapping.sam
$(HONEY_DIR)/%.tails.unsrtbam : $(HONEY_DIR)/%.bam $(CBS6039_V3)
	echo "Extracting Tails"
	$(setpath) ; Honey.py pie $(word 1,$^) $(word 2,$^)
	mv $(basename $@).bam $@

$(HONEY_DIR)/%.final.bam : $(HONEY_DIR)/%.tails.bam $(CBS6039_V3)
	echo "Calling MD Tag"
	samtools calmd -b $(word 1,$^) $(word 2,$^) > $@.tmp
	mv $@.tmp $@

$(HONEY_DIR)/%.final.hon.tails : $(HONEY_DIR)/%.final.bam
	echo "Calling Tails"
	$(setpath) ; Honey.py tails $(word 1,$^)

$(HONEY_DIR)/%.final.hon.spots : $(HONEY_DIR)/%.final.bam
	echo "Calling Spots"
	$(setpath) ; Honey.py spots $(word 1,$^)
#------------------------------
# Utility Functions
#------------------------------
%.unsrtbam : %.sam
	$(dir_guard)
	samtools view -Sb $< > $@.tmp
	mv $@.tmp $@

%.bam : %.unsrtbam
	$(dir_guard)
	$(eval TMPOUT := $(basename $@)_TMP)
	samtools sort $<  $(TMPOUT)
	mv $(TMPOUT).bam $@

%.bam.bai : %.bam
	samtools index $<

##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
allclean : cleanout cleanmum
	rm -rf $(DATA_DIR)

cleanout :
	rm -rf $(OUT_DIR)

cleanmum :
	rm -f $(MUMMER_PREFIX).delta $(MUMMER_PREFIX).btab $(CBS6039_V3)

cleanhoney :
	# rm -f $(HONEY_DIR)/cbs6039_unc_pacbio.sam
	rm -f $(HONEY_DIR)/*.unsrtbam
	rm -f $(HONEY_DIR)/*.bam
	rm -f $(HONEY_DIR)/*.bai
	rm -f $(HONEY_DIR)/*.final.hon.*
