# See "Re-annotation Using MAKER Derived GFF3"
# https://groups.google.com/forum/#!searchin/maker-devel/est_forward/maker-devel/q9fxXGKO8mk/0ATwhJvZeI4J
# https://groups.google.com/forum/#!topic/maker-devel/SainvaQjP74
# http://www.yandell-lab.org/software/maker.html

# https://github.com/sjackman/docker-bio/blob/master/maker/Dockerfile

# Tutorials
# http://onlinelibrary.wiley.com/doi/10.1002/0471250953.bi0411s48/full
# http://gmod.org/wiki/MAKER_Tutorial
# http://gmod.org/wiki/MAKER_Tutorial#Re-annotation_Using_MAKER_Derived_GFF3


# docker run --rm -it -v $HOME/cram_genomes:/home/linuxbrew/cram_genomes sjackman/maker
# docker run --rm -it --entrypoint=/bin/bash -v $HOME/cram_genomes:/home/linuxbrew/cram_genomes sjackman/maker

# sudo mount -t cifs  //oit-nas-nb12pub.oit.duke.edu/scratch/test_rapid_scratch /mnt/test_rapid_scratch --rw  -o username=josh,noexec,file_mode=0666,dir_mode=0777


maker -CTL


# wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/720/235/GCA_001720235.1_Cryp_amyl_CBS6273_V2/GCA_001720235.1_Cryp_amyl_CBS6273_V2_rna_from_genomic.fna.gz
# wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/720/235/GCA_001720235.1_Cryp_amyl_CBS6273_V2/GCA_001720235.1_Cryp_amyl_CBS6273_V2_cds_from_genomic.fna.gz
# wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/720/235/GCA_001720235.1_Cryp_amyl_CBS6273_V2/GCA_001720235.1_Cryp_amyl_CBS6273_V2_feature_table.txt.gz
# wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/720/235/GCA_001720235.1_Cryp_amyl_CBS6273_V2/GCA_001720235.1_Cryp_amyl_CBS6273_V2_genomic.fna.gz
# wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/720/235/GCA_001720235.1_Cryp_amyl_CBS6273_V2/GCA_001720235.1_Cryp_amyl_CBS6273_V2_genomic.gff.gz

