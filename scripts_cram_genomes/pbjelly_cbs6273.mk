# make all TRUNCATE="400"  -l 8 -j 
# /ssh:gems:/home/josh/collabs/HeitmanLab/camy_centromere/cram_genomes/scripts_cram_genomes/pbjelly_cbs6273.mk
# make -l 8 -j -f /home/josh/collabs/HeitmanLab/camy_centromere/cram_genomes/scripts_cram_genomes/pbjelly_cbs6273.mk all
# make -l 8 -j -f /home/josh/collabs/HeitmanLab/camy_centromere/cram_genomes/scripts_cram_genomes/pbjelly_cbs6273.mk all TRUNCATE="400"
# make -l 8 -j -f /home/josh/collabs/HeitmanLab/camy_centromere/cram_genomes/scripts_cram_genomes/pbjelly_cbs6273.mk pristine

MUMMER_DIR=$(COLLAB)/software/MUMmer3.23

NUMTHREADS ?= 10

RANDOM_SEED ?= 0

DATA_DIR := data
OUT_DIR := out
RAW_DIR := /nfs/gems_sata/heitman/camy/camy_raw_data/pacbio
MAPPING_DIR := $(OUT_DIR)/mapping
SUPPORT_DIR := $(OUT_DIR)/support

ORIGINAL_INPUT_GENOME ?= $(CAMY)/cram_genomes/revised_genomes/cbs6273_scaffolds_v4.fasta
REF := $(DATA_DIR)/$(notdir $(ORIGINAL_INPUT_GENOME))
REF_INDEX=$(addsuffix .sa,$(REF))

PACBIO_1_GZ := $(RAW_DIR)/cbs6273_unc1_filtered_subreads.fastq.gz
PACBIO_2_GZ := $(RAW_DIR)/cbs6273_unc2_filtered_subreads.fastq.gz
PACBIO_1 := $(DATA_DIR)/$(basename $(notdir $(PACBIO_1_GZ)))
PACBIO_2 := $(DATA_DIR)/$(basename $(notdir $(PACBIO_2_GZ)))

# PROTOCOL := $(CAMY)/cram_genomes/config/pbjelly_cbs6273_makefile.xml
PROTOCOL := pbjelly_cbs6273_makefile.xml

PB_FASTQS = $(PACBIO_2) $(PACBIO_1)
MAPPING_FILES := $(addprefix $(MAPPING_DIR),$(addsuffix .m4,$(notdir $(PB_FASTQS))))
SUPPORT_FILES := $(addprefix $(SUPPORT_DIR),$(addsuffix .gml,$(notdir $(PB_FASTQS))))
GAP_FILLED_FASTA := $(OUT_DIR)/jelly.out.fasta
# 6273_GENOME_PRE5A := cbs6273_genome_PRE5a.fasta
# 6273_GENOME_PRE5A_MD5 := cbs6273_genome_PRE5a.fasta.md5
6273_GENOME_PRE5B := cbs6273_genome_PRE5b.fasta
6273_GENOME_PRE5B_MD5 := cbs6273_genome_PRE5b.fasta.md5
OUTPUT_GENOME ?= $(RESULTS_DIR)/cbs6273_genome_5a.fasta
OUTPUT_GENOME_MD5 ?= $(OUTPUT_GENOME).md5

MUMMER_PREFIX:= cbs6273_genome_v4_to_v5

dir_guard=@mkdir -p $(@D)

BLASR_PATH=/opt/blasr/alignment/bin/
# PB_SETUP=/home/josh/collabs/software/PBSuite_14.1.15/setup.sh
PB_SETUP=/home/josh/collabs/software/PBSuite_15.8.24/setup.sh
# PB_SETUP=/home/josh/collabs/software/original_pbjelly_svn/pb-jelly-code-0/setup.sh
setpath=@export PATH=$(PATH):$(BLASR_PATH); source $(PB_SETUP)

# .ONESHELL:
.PRECIOUS :
.SECONDARY : 

all : pbjelly

mummer_stuff : $(6273_GENOME_PRE5B) $(6273_GENOME_PRE5B_MD5)

pbjelly : $(GAP_FILLED_FASTA) $(OUTPUT_GENOME) $(OUTPUT_GENOME_MD5)

todo :
	@echo "Nothing TODO right now"

$(REF) : $(ORIGINAL_INPUT_GENOME)
	$(dir_guard)
	ln -s $< $@

# TRUNCATE=
$(DATA_DIR)/%.fastq : $(RAW_DIR)/%.fastq.gz
ifdef TRUNCATE
	$(dir_guard)
	@echo "Truncating FASTQs to $(TRUNCATE) lines"
	gzip -dc < $< | head -n $(TRUNCATE) > $@
else
	$(dir_guard)
	@echo "Using full length FASTQs"
	gzip -dc < $< > $@
endif

setup : $(REF_INDEX)

%.fasta.sa : $(PROTOCOL) %.fasta $(PB_FASTQS)
	$(setpath) ; Jelly.py setup $<

mapping : $(MAPPING_FILES)

#--------------------------------------------------
$(MAPPING_FILES) : $(MAPPING_DIR)/mapping.stamp

.INTERMEDIATE: $(MAPPING_DIR)/mapping.stamp
$(MAPPING_DIR)/mapping.stamp : $(PROTOCOL) $(REF_INDEX) $(PB_FASTQS)
	$(dir_guard)
	@rm -f $@.tmp
	@touch $@.tmp
	$(setpath) ; Jelly.py mapping $<
	@mv -f $@.tmp $@

support : $(SUPPORT_FILES)

$(SUPPORT_FILES) : $(SUPPORT_DIR)/support.stamp

.INTERMEDIATE: $(SUPPORT_DIR)/support.stamp
$(SUPPORT_DIR)/support.stamp : $(PROTOCOL) $(REF_INDEX) $(MAPPING_FILES)
	$(dir_guard)
	@rm -f $@.tmp
	@touch $@.tmp
	$(setpath) ; Jelly.py support $<
	@mv -f $@.tmp $@

#--------------------------------------------------
EXTRACTION_FILE = $(OUT_DIR)/assembly/masterSupport.bml
extraction : $(EXTRACTION_FILE)

$(EXTRACTION_FILE) : $(PROTOCOL) $(REF_INDEX) $(SUPPORT_FILES)
	$(dir_guard)
	$(setpath) ; Jelly.py extraction $<

# PB_FINAL_FILES := $(addprefix $(OUT_DIR)/, gap_fill_status.txt  jelly.out.fasta  jelly.out.qual  liftOverTable.json)
#--------------------------------------------------
$(GAP_FILLED_FASTA) : $(PROTOCOL) $(REF_INDEX) $(EXTRACTION_FILE)
	# $(dir_guard)
	$(setpath) ; Jelly.py assembly $< -x "--random_seed $(RANDOM_SEED)"
	# $(setpath) ; Jelly.py assembly $<
	$(setpath) ; Jelly.py output $<
#--------------------------------------------------
##======================================================================
##======================================================================
%.btab : %.delta
	$(MUMMER_DIR)/show-coords -B -r $< > $@.tmp
	mv $@.tmp  $@

$(MUMMER_PREFIX).delta : $(ORIGINAL_INPUT_GENOME) $(OUTPUT_GENOME)
	$(MUMMER_DIR)/nucmer --maxgap=500 --mincluster=100 --prefix=$(basename $@)_TMP $(word 1,$^) $(word 2,$^)
	mv $(basename $@)_TMP.delta $@

$(6273_GENOME_PRE5B) : $(MUMMER_PREFIX).btab $(OUTPUT_GENOME)
	python2.7 $(SCRIPTS)/match_sequences.py $(word 1,$^) --rename $(word 2,$^) --out $@.tmp
	mv $@.tmp $@
##======================================================================
##======================================================================

$(OUTPUT_GENOME) : $(GAP_FILLED_FASTA)
	# cp -a $< $@
	$(dir_guard)
	python2.7 $(SCRIPTS)/reformat_fasta.py --size_sort $< --output $@

%.md5 : %
	$(dir_guard)
	md5sum $< > $@

##============================================================
## Generate Protocol Config File
##============================================================

$(PROTOCOL) : $(PB_FASTQS)
	@echo "<!-- PBSuite v14.1.15-->" > $@
	@echo "<!-- Run with $$CAMY/cram_scripts/pbjelly_cbs6273.sh -->" >> $@
	@echo "<jellyProtocol>" >> $@
	@echo "    <reference>$(REF)</reference>  " >> $@
	@echo "    <outputDir>$(OUT_DIR)</outputDir>" >> $@
	@echo "<!-- " >> $@
	@echo "    <cluster>" >> $@
	@echo "        <command notes="For single node, multi-core machines" >$${CMD} $${JOBNAME} 2> $${STDERR} 1> $${STDOUT} &amp;</command>" >> $@
	@echo "        <nJobs>$(NUMTHREADS)</nJobs>" >> $@
	@echo "    </cluster>" >> $@
	@echo "-->" >> $@
	@echo "    <blasr>-minMatch 8 -sdpTupleSize 8 -minPctIdentity 75 -bestn 1 -nCandidates 10 -maxScore -500 -nproc $(NUMTHREADS) -noSplitSubreads</blasr>" >> $@
	@echo "    <input baseDir=\"$(DATA_DIR)/\">" >> $@
	@echo "        <job>$(notdir $(PACBIO_1))</job>" >> $@
	@echo "        <job>$(notdir $(PACBIO_2))</job>" >> $@
	@echo "    </input>" >> $@
	@echo "</jellyProtocol>" >> $@

##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
cleandata : 
	rm -rf $(DATA_DIR)

cleanmum :
	rm -f $(MUMMER_PREFIX).delta $(MUMMER_PREFIX).btab $(CBS6039_V3)

cleanout :
	rm -rf $(OUT_DIR)

pristine : cleanout cleandata
	rm -f $(PROTOCOL) $(OUTPUT_GENOME) *.fasta *.md5
