where-am-i = $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THIS_MAKEFILE := $(call where-am-i)
NOW = $(shell date)
$(warning $(realpath $(THIS_MAKEFILE)))
$(warning $(abspath $(THIS_MAKEFILE)))
##------------------------------------------------------------
##------------------------------------------------------------
## MISC
##------------------------------------------------------------
NUMTHREADS ?= 4
MAXMEM := 10000000000
dir_guard=@mkdir -p $(@D)
NUM_JUNCTION_NS = 2000

.PRECIOUS: 

.SECONDARY: 
##------------------------------------------------------------
## DIRS
##------------------------------------------------------------
GENOME_DIR := genome
TMP_DIR := tmp
FINAL_GENOME_DIR := $(CAMY)/cram_genomes/revised_genomes

##------------------------------------------------------------
## INPUT FILES
##------------------------------------------------------------
6039_GENOME_V3 := $(CAMY)/cram_genomes/revised_genomes/cbs6039_genome_v3.fasta
6273_GENOME_V3 := $(CAMY)/cram_genomes/Broad_CRAM_assemblies/Assemblies/cbs6273_v3.scaffolds.fasta

##------------------------------------------------------------
## OUTPUT FILES
##------------------------------------------------------------

## CBS6039
6039_SCAF8_SCAF14_FUSION := $(TMP_DIR)/scaf8_scaf14_fusion.fa
6039_GENOME_V3_SCAF8 := $(TMP_DIR)/Cryptococcus_amylolentus_scaffold_8.fa
6039_GENOME_V3_SCAF14 := $(TMP_DIR)/Cryptococcus_amylolentus_scaffold_14.fa
6039_GENOME_V3_OTHERS := $(TMP_DIR)/6039_v3_other_scafs.fa
6039_GENOME_PRE4A := cbs6039_genome_PRE4a.fasta
6039_GENOME_PRE4A_REVCOMP_SCAFS := cbs6039_genome_PRE4a_rcscafs.fasta
6039_GENOME_PRE4A_OTHER_SCAFS := cbs6039_genome_PRE4a_other_scafs.fasta
6039_GENOME_PRE4A_SCAFS_TO_RC := $(TMP_DIR)/cbs6039_genome_PRE4a_scafs_to_rc.fasta
6039_GENOME_PRE4B := cbs6039_genome_PRE4b.fasta
6039_GENOME_PRE4B_SORTED := cbs6039_genome_PRE4b.sorted.fasta
6039_GENOME_V4 := $(FINAL_GENOME_DIR)/cbs6039_scaffolds_v4.fasta


## CBS6273
6273_SCAF14_SCAF15_FUSION := $(TMP_DIR)/scaf14_scaf15rc_fusion.fa
6273_GENOME_V3_SCAF14 := $(TMP_DIR)/scaffold00014.fa
6273_GENOME_V3_SCAF15 := $(TMP_DIR)/scaffold00015.fa
6273_GENOME_V3_SCAF15_RC := $(TMP_DIR)/scaffold00015rc.fa
6273_GENOME_V3_OTHERS := $(TMP_DIR)/6273_v3_other_scafs.fa
6273_GENOME_PRE4A := cbs6273_genome_PRE4a.fasta
6273_GENOME_PRE4A_SORTED := cbs6273_genome_PRE4a.sorted.fasta
6273_GENOME_V4 := $(FINAL_GENOME_DIR)/cbs6273_scaffolds_v4.fasta

##------------------------------------------------------------
all :  v4_genomes

v4_genomes : cbs6039_v4 cbs6273_v4

pre_v4_genomes : $(6273_GENOME_PRE4A_SORTED) $(6039_GENOME_PRE4B_SORTED)

cbs6039_v4 : $(6039_GENOME_V4)

cbs6273_v4 : $(6273_GENOME_V4)

fuse_scafs : $(6039_SCAF8_SCAF14_FUSION) $(6273_SCAF14_SCAF15_FUSION)

current : $(6273_GENOME_V3_SCAF15_RC) $(6273_GENOME_V3_OTHERS)

#--------------------------------------------------------------------------------
##------------------------------------------------------------
## CBS6039
##------------------------------------------------------------
# 1. Extract Cryptococcus_amylolentus_scaffold_8 and Cryptococcus_amylolentus_scaffold_14 from cbs6039_genome_v3.fasta
$(TMP_DIR)/Cryptococcus_amylolentus_scaffold_%.fa : $(6039_GENOME_V3)
	$(dir_guard)
	python2.7 $(SCRIPTS)/reformat_fasta.py --extract $(basename $(notdir $@)) --output $(basename $@)_tmp.fasta $<
	mv $(basename $@)_tmp.fasta $@

# 2. use concatenate_sequences.py to concatenate 8 and 14
$(6039_SCAF8_SCAF14_FUSION) : $(6039_GENOME_V3_SCAF8) $(6039_GENOME_V3_SCAF14)
	$(dir_guard)
	python2.7 $(SCRIPTS)/concatenate_sequences.py --character n --njunction $(NUM_JUNCTION_NS) --id Cryptococcus_amylolentus_scaffold_814 -o $(basename $@)_tmp.fasta $^
	mv $(basename $@)_tmp.fasta $@

# 3. Extract Cryptococcus_amylolentus_scaffold_XX for 1-7, 9-13, 15-16
$(6039_GENOME_V3_OTHERS) : $(6039_GENOME_V3)
	$(dir_guard)
	python2.7 $(SCRIPTS)/reformat_fasta.py --output $(basename $@)_tmp.fasta $< \
		--extract Cryptococcus_amylolentus_scaffold_1 \
		--extract Cryptococcus_amylolentus_scaffold_2 \
		--extract Cryptococcus_amylolentus_scaffold_3 \
		--extract Cryptococcus_amylolentus_scaffold_4 \
		--extract Cryptococcus_amylolentus_scaffold_5 \
		--extract Cryptococcus_amylolentus_scaffold_6 \
		--extract Cryptococcus_amylolentus_scaffold_7 \
		--extract Cryptococcus_amylolentus_scaffold_9 \
		--extract Cryptococcus_amylolentus_scaffold_10 \
		--extract Cryptococcus_amylolentus_scaffold_11 \
		--extract Cryptococcus_amylolentus_scaffold_12 \
		--extract Cryptococcus_amylolentus_scaffold_13 \
		--extract Cryptococcus_amylolentus_scaffold_15 \
		--extract Cryptococcus_amylolentus_scaffold_16
	mv $(basename $@)_tmp.fasta $@

# 4. cat 8+14 with other chromosomes
$(6039_GENOME_PRE4A) : $(6039_SCAF8_SCAF14_FUSION) $(6039_GENOME_V3_OTHERS)
	$(dir_guard)
	cat $^ > $@

%.sorted.fasta : %.fasta
	$(dir_guard)
	python2.7 $(SCRIPTS)/reformat_fasta.py --size_sort --output $(basename $@)_tmp.fasta $<
	mv $(basename $@)_tmp.fasta $@

#----------------
$(6039_GENOME_PRE4A_SCAFS_TO_RC) : $(6039_GENOME_PRE4A)
	$(dir_guard)
	python2.7 $(SCRIPTS)/reformat_fasta.py $< --output $(basename $@)_tmp.fasta \
		--extract Cryptococcus_amylolentus_scaffold_814 \
		--extract Cryptococcus_amylolentus_scaffold_1 \
		--extract Cryptococcus_amylolentus_scaffold_2 \
		--extract Cryptococcus_amylolentus_scaffold_3 \
		--extract Cryptococcus_amylolentus_scaffold_5 \
		--extract Cryptococcus_amylolentus_scaffold_10 \
		--extract Cryptococcus_amylolentus_scaffold_12 \
		--extract Cryptococcus_amylolentus_scaffold_13
	mv $(basename $@)_tmp.fasta $@

$(6039_GENOME_PRE4A_REVCOMP_SCAFS) : $(6039_GENOME_PRE4A_SCAFS_TO_RC)
	$(dir_guard)
	python2.7 $(SCRIPTS)/reverse_complement.py $< --output $(basename $@)_tmp.fasta
	mv $(basename $@)_tmp.fasta $@

#----------------
$(6039_GENOME_PRE4A_OTHER_SCAFS) : $(6039_GENOME_PRE4A)
	$(dir_guard)
	python2.7 $(SCRIPTS)/reformat_fasta.py --output $(basename $@)_tmp.fasta $< \
		--extract Cryptococcus_amylolentus_scaffold_4 \
		--extract Cryptococcus_amylolentus_scaffold_6 \
		--extract Cryptococcus_amylolentus_scaffold_7 \
		--extract Cryptococcus_amylolentus_scaffold_9 \
		--extract Cryptococcus_amylolentus_scaffold_11 \
		--extract Cryptococcus_amylolentus_scaffold_15 \
		--extract Cryptococcus_amylolentus_scaffold_16
	mv $(basename $@)_tmp.fasta $@

$(6039_GENOME_PRE4B) : $(6039_GENOME_PRE4A_OTHER_SCAFS) $(6039_GENOME_PRE4A_REVCOMP_SCAFS)
	$(dir_guard)
	cat $^ > $@

$(6039_GENOME_V4) : $(6039_GENOME_PRE4B_SORTED)
	cp -a $< $@

##------------------------------------------------------------
## CBS6273
##------------------------------------------------------------
# 1. Extract scaffold00014 and Cryptococcus_amylolentus_scaffold_14 from cbs6039_genome_v3.fasta
$(TMP_DIR)/scaffold000%.fa : $(6273_GENOME_V3)
	$(dir_guard)
	python2.7 $(SCRIPTS)/reformat_fasta.py --extract $(basename $(notdir $@)) --output $(basename $@)_tmp.fasta $<
	mv $(basename $@)_tmp.fasta $@

$(6273_GENOME_V3_SCAF15_RC) : $(6273_GENOME_V3_SCAF15)
	$(dir_guard)
	python2.7 $(SCRIPTS)/reverse_complement.py $< --output $(basename $@)_tmp.fasta
	mv $(basename $@)_tmp.fasta $@

# 2. use concatenate_sequences.py to concatenate 14 and 15
$(6273_SCAF14_SCAF15_FUSION) : $(6273_GENOME_V3_SCAF14) $(6273_GENOME_V3_SCAF15_RC)
	$(dir_guard)
	python2.7 $(SCRIPTS)/concatenate_sequences.py --character n --njunction $(NUM_JUNCTION_NS) --id scaffold14015 -o $(basename $@)_tmp.fasta $^
	mv $(basename $@)_tmp.fasta $@

$(6273_GENOME_V3_OTHERS) : $(6273_GENOME_V3)
	$(dir_guard)
	python2.7 $(SCRIPTS)/reformat_fasta.py --output $(basename $@)_tmp.fasta $< \
		--extract scaffold00001 \
		--extract scaffold00002 \
		--extract scaffold00003 \
		--extract scaffold00004 \
		--extract scaffold00005 \
		--extract scaffold00006 \
		--extract scaffold00007 \
		--extract scaffold00008 \
		--extract scaffold00009 \
		--extract scaffold00010 \
		--extract scaffold00011 \
		--extract scaffold00012 \
		--extract scaffold00013 \
		--extract scaffold00016 \
		--extract scaffold00017 \
		--extract scaffold00018
	mv $(basename $@)_tmp.fasta $@

# 4. cat 8+14 with other chromosomes
$(6273_GENOME_PRE4A) : $(6273_SCAF14_SCAF15_FUSION) $(6273_GENOME_V3_OTHERS)
	$(dir_guard)
	cat $^ > $@

$(6273_GENOME_V4) : $(6273_GENOME_PRE4A)
	cp -a $< $@

# ###=============================================================================
# ## Cleanup
# ###=============================================================================

clean :
	rm -rf $(TMP_DIR)
