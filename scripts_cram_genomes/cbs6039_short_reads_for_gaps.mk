## make -f $CAMY/cram_scripts/cbs6039_short_reads_for_gaps.mk --jobs 8 --load-average 12
## make --dry-run -f $CAMY/cram_scripts/cbs6039_short_reads_for_gaps.mk --jobs 3 --load-average 18 SOURCE_GENOME=/home/josh/collabs/HeitmanLab/camy_centromere/pcr_data/cbs6039_scaffolds_v5a.fasta NUMTHREADS=4
## make --dry-run -f $CAMY/cram_scripts/cbs6039_short_reads_for_gaps.mk --jobs 3 --load-average 18 NUMTHREADS=4
## /ssh:gems:/home/josh/collabs/HeitmanLab/camy_centromere/cram_scripts/cbs6039_short_reads_for_gaps.mk

## fastq-dump --split-files --gzip SRR949802 --outdir broad_data/
## fastq-dump --split-files --gzip SRR949803 --outdir broad_data/
## fastq-dump --split-files --gzip SRR949804 --outdir broad_data/
## fastq-dump --split-files --gzip SRR949805 --outdir broad_data/
##
## mkdir raw_fastqs
## ln -s ls /nfs/gems_sata/heitman/camy/public_rawdata/broad_cbs6039_rawdata/SRR94980?_?.fastq.gz raw_fastqs
## ln -s /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs/ss1_ATCACG_s_6_?.fastq.gz raw_fastqs


##------------------------------------------------------------
## DIRS
##------------------------------------------------------------
# RAW_FASTQ_DIR := /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs
RAW_FASTQ_DIR := raw_fastqs
TEMP_DIR := temp
FINAL_FASTQ_DIR := final_fastqs
GENOME_DIR := genome
BWA_OUTDIR := bams
INFO_DIR := info
PILON_DIR := pilon
PILON_JAR_DIR := $(COLLAB)/software

##------------------------------------------------------------

##------------------------------------------------------------
## Files
##------------------------------------------------------------
# SOURCE_GENOME ?= $(CAMY)/cram_genomes/revised_genomes/cbs6039_scaffolds_v4.fasta
SOURCE_GENOME ?= $(CAMY)/pcr_data/cbs6039_scaffolds_v5b.fasta

ALL_ADAPTER_FASTA = $(COLLAB)/docs/illumina_adapters.fasta
ADAPTER_FASTA = $(INFO_DIR)/illumina_adapter.fasta

# RAW_FASTQ_R1 := $(RAW_FASTQ_DIR)/ss1_ATCACG_s_6_1.fastq.gz
# RAW_FASTQ_R2 := $(RAW_FASTQ_DIR)/ss1_ATCACG_s_6_2.fastq.gz

RAW_FASTQ_R1s := $(wildcard $(RAW_FASTQ_DIR)/*_1.fastq.gz)
RAW_FASTQ_R2s := $(wildcard $(RAW_FASTQ_DIR)/*_2.fastq.gz)
RAW_FASTQS := $(RAW_FASTQ_R1s) $(RAW_FASTQ_R2s)

# $(addprefix prefix,names...) 
TRIM_FASTQS := $(addprefix $(FINAL_FASTQ_DIR)/,$(notdir $(RAW_FASTQS:.fastq.gz=.trim.fastq.gz)))

SANGER_FASTQ := $(RAW_FASTQ_DIR)/CBS6039_gap_sanger_raw.fastq
SANGER_BAM := $(addprefix $(BWA_OUTDIR)/,$(notdir $(SANGER_FASTQ:.fastq=.bam)))

FINAL_BAMS := $(addprefix $(BWA_OUTDIR)/,$(notdir $(RAW_FASTQ_R1s:_1.fastq.gz=.bam))) # $(SANGER_BAM)
BAMS_INDICES := $(FINAL_BAMS:.bam=.bam.bai)


GENOME = $(GENOME_DIR)/$(notdir $(SOURCE_GENOME:.fasta=.fa))

INDEXBASE=$(basename $(GENOME))
INDEX_FILES=$(addprefix $(INDEXBASE),.bwt .pac .ann .amb .sa)

PILON_BASE:= $(PILON_DIR)/$(notdir $(basename $(SOURCE_GENOME)))
PILON_FASTA:=$(PILON_BASE).pilon.fasta
PILON_CHANGES:=$(PILON_BASE).pilon.changes

.PRECIOUS:

.SECONDARY:

##------------------------------------------------------------
JAVA_MAXMEM ?= 10000
NUMTHREADS ?= 10
SAMTOOLS_MAXMEM := 10G
##------------------------------------------------------------
dir_guard=@mkdir -p $(@D)
##------------------------------------------------------------
all : $(BAMS_INDICES) $(PILON_FASTA) $(PILON_CHANGES) # $(FINAL_FASTQS) 

test :
	echo "RAW_FASTQ_R1s" $(RAW_FASTQ_R1s)
	echo "RAW_FASTQ_R2s" $(RAW_FASTQ_R2s)
	echo "RAW_FASTQS" $(RAW_FASTQS)
	echo "TRIM_FASTQS" $(TRIM_FASTQS)
	echo "FINAL_BAMS" $(FINAL_BAMS)
	echo "BAMS_INDICES" $(BAMS_INDICES)

run_pilon : $(PILON_FASTA) $(PILON_CHANGES)
	echo $(PILON_FASTA) $(PILON_CHANGES)

##------------------------------------------------------------
$(ADAPTER_FASTA) : $(ALL_ADAPTER_FASTA)
	$(dir_guard)
	grep -B1 ATCACG $< > $@

##------------------------------------------------------------
# Trim adapters
$(FINAL_FASTQ_DIR)/%_1.trim.fastq.gz $(FINAL_FASTQ_DIR)/%_2.trim.fastq.gz : $(ADAPTER_FASTA) $(RAW_FASTQ_DIR)/%_1.fastq.gz $(RAW_FASTQ_DIR)/%_2.fastq.gz
	$(dir_guard)
	fastq-mcf $(word 1,$^) $(word 2,$^) $(word 3,$^) -o $(@D)/$*_1.tmp.gz -o $(@D)/$*_2.tmp.gz
	mv $(@D)/$*_1.tmp.gz $(@D)/$*_1.trim.fastq.gz
	mv $(@D)/$*_2.tmp.gz $(@D)/$*_2.trim.fastq.gz
#--------------------------------------------------------------------------------
$(BWA_OUTDIR)/%.bam : $(FINAL_FASTQ_DIR)/%_1.trim.fastq.gz $(FINAL_FASTQ_DIR)/%_2.trim.fastq.gz $(INDEX_FILES)
	$(dir_guard)
	bwa mem -t $(NUMTHREADS) $(INDEXBASE) $(word 1,$^) $(word 2,$^) | samtools view -Sb - | samtools sort -m $(SAMTOOLS_MAXMEM) - $(@D)/$*.tmp
	mv $(@D)/$*.tmp.bam $@

#--------------------------------------------------------------------------------
$(BWA_OUTDIR)/%.bam : $(RAW_FASTQ_DIR)/%.fastq
	$(dir_guard)
	bwa mem -t $(NUMTHREADS) $(INDEXBASE) $(word 1,$^) | samtools view -Sb - | samtools sort -m $(SAMTOOLS_MAXMEM) - $(@D)/$*.tmp
	mv $(@D)/$*.tmp.bam $@

%.bam.bai : %.bam
	samtools index $<
#--------------------------------------------------------------------------------
# Index reference genome
%.bwt %.pac %.ann %.amb %.sa : %.fa
	bwa index $< -p $*

$(GENOME) : $(SOURCE_GENOME)
	$(dir_guard)
	ln -s $< $@

#--------------------------------------------------------------------------------
# Pilon
#--------------------------------------------------------------------------------
# java -jar /home/josh/collabs/software/pilon-1.16.jar --genome genome/cbs6039_scaffolds_v5a.fa --frags bams/ss1_ATCACG_s_6.bam --jumps bams/SRR949802.bam --jumps bams/SRR949803.bam --frags bams/SRR949804.bam --frags bams/SRR949805.bam --output pilon/cbs6039_scaffolds_v5a_pilon --changes

# java -jar /home/josh/collabs/software/pilon-1.16.jar --genome genome/cbs6039_scaffolds_v5a.fa --frags bams/ss1_ATCACG_s_6.bam --jumps bams/SRR949802.bam --jumps bams/SRR949803.bam --frags bams/SRR949804.bam --frags bams/SRR949805.bam  --unpaired bams/CBS6039_gap_sanger_raw.bam  --output pilon_with_sanger/cbs6039_scaffolds_v5a_pilon --changes

# java -jar /home/josh/collabs/software/pilon-1.16.jar --genome $(CAMY)/pcr_data/cbs6039_scaffolds_v5b.fasta --frags bams/ss1_ATCACG_s_6.bam --jumps bams/SRR949802.bam --jumps bams/SRR949803.bam --frags bams/SRR949804.bam --frags bams/SRR949805.bam  --unpaired bams/CBS6039_gap_sanger_raw.bam  --output pilon/cbs6039_scaffolds_v5b_pilon --changes

# cbs6039_scaffolds_v5a_pilon.changes
# cbs6039_scaffolds_v5a_pilon.fasta

# $(PILON_DIR)/%.pilon.fasta $(PILON_DIR)/%.pilon.changes : $(GENOME_DIR)/%.fa $(BWA_OUTDIR)/ss1_ATCACG_s_6.bam $(BWA_OUTDIR)/SRR949802.bam $(BWA_OUTDIR)/SRR949803.bam $(BWA_OUTDIR)/SRR949804.bam $(BWA_OUTDIR)/SRR949805.bam $(BWA_OUTDIR)/CBS6039_gap_sanger_raw.bam $(BAMS_INDICES)
$(PILON_DIR)/%.pilon.fasta $(PILON_DIR)/%.pilon.changes : $(GENOME_DIR)/%.fa $(BWA_OUTDIR)/ss1_ATCACG_s_6.bam $(BWA_OUTDIR)/SRR949802.bam $(BWA_OUTDIR)/SRR949803.bam $(BWA_OUTDIR)/SRR949804.bam $(BWA_OUTDIR)/SRR949805.bam $(BAMS_INDICES)
	$(dir_guard)
	java -Djava.io.tmpdir=$(TEMP_DIR) -Xmx$(JAVA_MAXMEM)M -jar $(PILON_JAR_DIR)/pilon-1.16.jar --changes --genome $(word 1,$^) --frags $(word 2,$^) --jumps $(word 3,$^) --jumps $(word 4,$^) --frags $(word 5,$^) --frags $(word 6,$^) --output $(@D)/$*.pilon
