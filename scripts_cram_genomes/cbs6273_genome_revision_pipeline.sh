echo "A pipeline for improving the CBS6273 draft genome using PBJelly and Pilon"

BASEDIR=$PWD

MAKEFILE_DIR=$CAMY/cram_genomes/scripts_cram_genomes/

STARTING_GENOME=$CAMY/cram_genomes/revised_genomes/cbs6273_scaffolds_v4.fasta
CBS6039_GENOME=$CAMY/cram_genomes/final_genomes/cryptococcus_amylolentus_cbs_6039_final.fasta

PBJELLY_MAKEFILE=$MAKEFILE_DIR/pbjelly_cbs6273.mk
PILON_MAKEFILE=$MAKEFILE_DIR/cbs6273_pilon.mk


PATH="/home/josh/collabs/software/MUMmer3.23:$PATH"

TRUNCATE=
# TRUNCATE="1000"
# DRY_RUN="--dry-run"
DRY_RUN=""

# #===========================================================================
# # First Round: Run PBJelly
# #===========================================================================

# CURRENT_INPUT=$STARTING_GENOME
# CURRENT_OUTPUT=$BASEDIR/cbs6273_genome_5a.fasta
# CURRENT_DIR="first_round_dir"
# #-----------------------------
# mkdir -p $CURRENT_DIR
# cd $CURRENT_DIR
# make $DRY_RUN -f $PBJELLY_MAKEFILE ORIGINAL_INPUT_GENOME=$CURRENT_INPUT OUTPUT_GENOME=$CURRENT_OUTPUT TRUNCATE=$TRUNCATE
# cd $BASEDIR

#===========================================================================
# Second Round: Run Pilon
#===========================================================================
CURRENT_INPUT=$STARTING_GENOME
CURRENT_OUTPUT=$BASEDIR/cbs6273_genome_5b.fasta
CURRENT_DIR="second_round_dir"
#-----------------------------
mkdir -p $CURRENT_DIR
cd $CURRENT_DIR
make $DRY_RUN -f $PILON_MAKEFILE ORIGINAL_INPUT_GENOME=$CURRENT_INPUT OUTPUT_GENOME=$CURRENT_OUTPUT  --jobs 3 --load-average 18 NUMTHREADS=8 --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_L004_R1_001.fastq.gz --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_s_6_2.fastq.gz TRUNCATE=$TRUNCATE
cd $BASEDIR

#===========================================================================
# Third Round: Run PBJelly
#===========================================================================
CURRENT_INPUT=$CURRENT_OUTPUT
CURRENT_OUTPUT=$BASEDIR/cbs6273_genome_5c.fasta
CURRENT_DIR="third_round_dir"
#-----------------------------
if [ -n "$TRUNCATE" ];
then THIRD_ROUND_TRUNCATE=$(($TRUNCATE * 4));
fi

mkdir -p $CURRENT_DIR
cd $CURRENT_DIR
make $DRY_RUN -f $PBJELLY_MAKEFILE ORIGINAL_INPUT_GENOME=$CURRENT_INPUT OUTPUT_GENOME=$CURRENT_OUTPUT TRUNCATE=$THIRD_ROUND_TRUNCATE
cd $BASEDIR

#===========================================================================
# Fourth Round: Make Chrom Orientation Congruent to CBS6039
#===========================================================================
CURRENT_INPUT=$CURRENT_OUTPUT
CURRENT_OUTPUT=$BASEDIR/cbs6273_genome_5d.fasta
CURRENT_DIR="fourth_round_dir"
#-----------------------------
mkdir -p $CURRENT_DIR
cd $CURRENT_DIR
python2.7 $SCRIPTS/match_sequences.py --generate_btab $CBS6039_GENOME $CURRENT_INPUT --make_congruent $CURRENT_INPUT --output $CURRENT_OUTPUT
cd $BASEDIR

#===========================================================================
# Fifth Round: Rename Chroms
#===========================================================================
CURRENT_INPUT=$CURRENT_OUTPUT
CURRENT_OUTPUT=$BASEDIR/cbs6273_genome_5e.fasta
CURRENT_DIR="fifth_round_dir"
#-----------------------------
mkdir -p $CURRENT_DIR
cd $CURRENT_DIR
python2.7 $SCRIPTS/reformat_fasta.py --name_by_size CBS6273_chrom $CURRENT_INPUT --output $CURRENT_OUTPUT
cd $BASEDIR
