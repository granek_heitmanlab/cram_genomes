## /ssh:gems:/home/josh/collabs/HeitmanLab/camy_centromere/cram_genomes/scripts_cram_genomes/cbs6273_short_reads_for_gaps.mk ## make --dry-run -f $CAMY/cram_scripts/cbs6273_short_reads_for_gaps.mk --jobs 3 --load-average 18 ORIGINAL_INPUT_GENOME=/home/josh/collabs/HeitmanLab/camy_centromere/pcr_data/cbs6273_scaffolds_v5a.fasta NUMTHREADS=4
## make --dry-run run_qc --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_L004_R1_001.fastq.gz --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_s_6_2.fastq.gz
## make --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_L004_R1_001.fastq.gz --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_s_6_2.fastq.gz --jobs 4 --load-average 18 NUMTHREADS=8
## make run_pilon  --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_L004_R1_001.fastq.gz --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_s_6_2.fastq.gz
## make --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_L004_R1_001.fastq.gz --assume-old /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs//ss2_CGATGT_s_6_2.fastq.gz


##------------------------------------------------------------
## INPUT DIRS
##------------------------------------------------------------
PILON_JAR_DIR := $(COLLAB)/software
ORIGINAL_LOCAL_FASTQ_DIR := /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs/


##------------------------------------------------------------
## OUTPUT DIRS
##------------------------------------------------------------
RAW_FASTQ_DIR := raw_fastqs
TEMP_DIR := temp
FINAL_FASTQ_DIR := final_fastqs
GENOME_DIR := genome
BWA_OUTDIR := bams
INFO_DIR := info
PILON_DIR := pilon
QC_DIR := qc
RESULTS_DIR := results
##------------------------------------------------------------

##------------------------------------------------------------
## Files
##------------------------------------------------------------
ORIGINAL_INPUT_GENOME ?= $(CAMY)/cram_genomes/revised_genomes/cbs6273_genome_PRE5a.fasta
OUTPUT_GENOME ?= $(RESULTS_DIR)/cbs6273_genome_5b.fasta

ALL_ADAPTER_FASTA = $(COLLAB)/docs/illumina_adapters.fasta
ADAPTER_FASTA = $(INFO_DIR)/illumina_adapter.fasta

# http://www.ncbi.nlm.nih.gov/sra?linkname=bioproject_sra_all&from_uid=191370
SRA_RUNS := SRR1010369 SRR998854
SRA_FASTQ_R1s := $(addprefix $(RAW_FASTQ_DIR)/, $(addsuffix _1.fastq.gz, $(SRA_RUNS)))
SRA_FASTQ_R2s := $(addprefix $(RAW_FASTQ_DIR)/, $(addsuffix _2.fastq.gz, $(SRA_RUNS)))

# /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs/ss2_CGATGT_s_6_2.fastq.gz
# /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs/ss2_CGATGT_s_6_1.fastq.gz
# /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs/ss2_CGATGT_L004_R1_001.fastq.gz
# /nfs/gems_sata/heitman/camy/camy_raw_data/camy_pe_fastqs/ss2_CGATGT_L004_R2_001.fastq.gz
LOCAL_FASTQ_R1s := $(addprefix $(RAW_FASTQ_DIR)/, $(addsuffix .fastq.gz, ss2_CGATGT_s_6_1 ss2_CGATGT_L004_1))
LOCAL_FASTQ_R2s := $(addprefix $(RAW_FASTQ_DIR)/, $(addsuffix .fastq.gz, ss2_CGATGT_s_6_2 ss2_CGATGT_L004_2))

RAW_FASTQ_R1s := $(SRA_FASTQ_R1s) $(LOCAL_FASTQ_R1s)
RAW_FASTQ_R2s := $(SRA_FASTQ_R2s) $(LOCAL_FASTQ_R2s)
RAW_FASTQS := $(RAW_FASTQ_R1s) $(RAW_FASTQ_R2s)

FASTQC_FILES := $(addprefix $(QC_DIR)/,$(notdir $(RAW_FASTQS:.fastq.gz=_fastqc.zip)))

TRIM_FASTQS := $(addprefix $(FINAL_FASTQ_DIR)/,$(notdir $(RAW_FASTQS:.fastq.gz=.trim.fastq.gz)))

FINAL_BAMS := $(addprefix $(BWA_OUTDIR)/,$(notdir $(RAW_FASTQ_R1s:_1.fastq.gz=.bam))) # $(SANGER_BAM)
BAMS_INDICES := $(FINAL_BAMS:.bam=.bam.bai)


LOCAL_INPUT_GENOME = $(GENOME_DIR)/$(notdir $(ORIGINAL_INPUT_GENOME:.fasta=.fa))

INDEXBASE=$(basename $(LOCAL_INPUT_GENOME))
INDEX_FILES=$(addprefix $(INDEXBASE),.bwt .pac .ann .amb .sa)

PILON_BASE:= $(PILON_DIR)/$(notdir $(basename $(ORIGINAL_INPUT_GENOME)))
PILON_FASTA:=$(PILON_BASE).pilon.fasta
PILON_CHANGES:=$(PILON_BASE).pilon.changes

.PRECIOUS:

.SECONDARY:

##------------------------------------------------------------
JAVA_MAXMEM ?= 40000
NUMTHREADS ?= 10
SAMTOOLS_MAXMEM := 5000000000
##------------------------------------------------------------
dir_guard=@mkdir -p $(@D)
##------------------------------------------------------------
all : $(FINAL_BAMS) $(BAMS_INDICES) $(PILON_FASTA) $(PILON_CHANGES) $(OUTPUT_GENOME) # $(FINAL_FASTQS) 

test :
	@echo "RAW_FASTQ_R1s" $(RAW_FASTQ_R1s)
	@echo "RAW_FASTQ_R2s" $(RAW_FASTQ_R2s)
	@echo "RAW_FASTQS" $(RAW_FASTQS)
	@echo "TRIM_FASTQS" $(TRIM_FASTQS)
	@echo "FINAL_BAMS" $(FINAL_BAMS)
	@echo "BAMS_INDICES" $(BAMS_INDICES)
ifdef TRUNCATE
	TRUNC_LINES=$(shell echo $$(($TRUNCATE*4)))
	@echo "Truncating FASTQs to:" $(TRUNCATE) $(TRUNC_LINES)
else
	@echo "Using full length FASTQs"
endif

download : $(SRA_FASTQ_R1s) $(SRA_FASTQ_R2s)
	echo $^

link : $(LOCAL_FASTQ_R1s) $(LOCAL_FASTQ_R2s)
	echo $^

trim : $(TRIM_FASTQS)

run_bwa : $(FINAL_BAMS) $(BAMS_INDICES)

run_pilon : $(PILON_FASTA) $(PILON_CHANGES)
	echo $(PILON_FASTA) $(PILON_CHANGES)

run_qc : $(FASTQC_FILES)

##------------------------------------------------------------
$(ADAPTER_FASTA) : $(ALL_ADAPTER_FASTA)
	$(dir_guard)
	egrep -B1 "CGATGT|GCCAAT|CTTGTA" $< | grep -v -- "--" > $@

###------------------
## Run FASTQC
###------------------
${QC_DIR}/%_fastqc.zip : $(RAW_FASTQ_DIR)/%.fastq.gz
	$(dir_guard)
	fastqc --outdir $(@D) --threads ${NUMTHREADS} --noextract $^

##------------------------------------------------------------
# Download Data From SRA
$(RAW_FASTQ_DIR)/SRR%_1.fastq.gz $(RAW_FASTQ_DIR)/SRR%_2.fastq.gz : 
	$(dir_guard)
	mkdir -p $(TEMP_DIR)
ifdef TRUNCATE
	@echo "Downloding first $(TRUNCATE) lines of FASTQs from SRA"
	fastq-dump -X $(TRUNCATE) --split-files --gzip --outdir $(TEMP_DIR) SRR$*
else
	@echo "Downloading full length FASTQs from SRA"
	fastq-dump --split-files --gzip --outdir $(TEMP_DIR) SRR$*
endif
	mv $(TEMP_DIR)/SRR$*_1.fastq.gz $(@D)
	mv $(TEMP_DIR)/SRR$*_2.fastq.gz $(@D)


##------------------------------------------------------------
# Link to Local FASTQs
$(RAW_FASTQ_DIR)/%_1.fastq.gz $(RAW_FASTQ_DIR)/%_2.fastq.gz : $(ORIGINAL_LOCAL_FASTQ_DIR)/%_1.fastq.gz $(ORIGINAL_LOCAL_FASTQ_DIR)/%_2.fastq.gz
	$(dir_guard)
ifdef TRUNCATE
	@echo "Truncating FASTQs to $(TRUNCATE) lines"
	gzip -dc < $(word 1,$^) | head -n $(TRUNCATE) | gzip -c > $(@D)/$*_1.fastq.gz
	gzip -dc < $(word 2,$^) | head -n $(TRUNCATE) | gzip -c > $(@D)/$*_2.fastq.gz
else
	@echo "Using full length FASTQs"
	ln -sf $(word 1,$^) $(@D)/$*_1.fastq.gz
	ln -sf $(word 2,$^) $(@D)/$*_2.fastq.gz
endif

$(RAW_FASTQ_DIR)/%_1.fastq.gz $(RAW_FASTQ_DIR)/%_2.fastq.gz : $(ORIGINAL_LOCAL_FASTQ_DIR)/%_R1_001.fastq.gz $(ORIGINAL_LOCAL_FASTQ_DIR)/%_R2_001.fastq.gz
	$(dir_guard)
ifdef TRUNCATE
	@echo "Truncating FASTQs to $(TRUNCATE) lines"
	gzip -dc < $(word 1,$^) | head -n $(TRUNCATE) | gzip -c > $(@D)/$*_1.fastq.gz
	gzip -dc < $(word 2,$^) | head -n $(TRUNCATE) | gzip -c > $(@D)/$*_2.fastq.gz
else
	@echo "Using full length FASTQs"
	ln -sf $(word 1,$^) $(@D)/$*_1.fastq.gz
	ln -sf $(word 2,$^) $(@D)/$*_2.fastq.gz
endif
##------------------------------------------------------------
# Trim adapters
$(FINAL_FASTQ_DIR)/%_1.trim.fastq.gz $(FINAL_FASTQ_DIR)/%_2.trim.fastq.gz : $(ADAPTER_FASTA) $(RAW_FASTQ_DIR)/%_1.fastq.gz $(RAW_FASTQ_DIR)/%_2.fastq.gz
	$(dir_guard)
	fastq-mcf $(word 1,$^) $(word 2,$^) $(word 3,$^) -o $(@D)/$*_1.tmp.gz -o $(@D)/$*_2.tmp.gz
	mv $(@D)/$*_1.tmp.gz $(@D)/$*_1.trim.fastq.gz
	mv $(@D)/$*_2.tmp.gz $(@D)/$*_2.trim.fastq.gz
#--------------------------------------------------------------------------------
$(BWA_OUTDIR)/%.bam : $(FINAL_FASTQ_DIR)/%_1.trim.fastq.gz $(FINAL_FASTQ_DIR)/%_2.trim.fastq.gz $(INDEX_FILES)
	$(dir_guard)
	bwa mem -t $(NUMTHREADS) $(INDEXBASE) $(word 1,$^) $(word 2,$^) | samtools view -Sb - | samtools sort -m $(SAMTOOLS_MAXMEM) - $(@D)/$*.tmp
	mv $(@D)/$*.tmp.bam $@
#--------------------------------------------------------------------------------
%.bam.bai : %.bam
	samtools index $<
#--------------------------------------------------------------------------------
# Index reference genome
%.bwt %.pac %.ann %.amb %.sa : %.fa
	bwa index $< -p $*

$(LOCAL_INPUT_GENOME) : $(ORIGINAL_INPUT_GENOME)
	$(dir_guard)
	ln -s $< $@

#--------------------------------------------------------------------------------
# Pilon
#--------------------------------------------------------------------------------

$(PILON_DIR)/%.pilon.fasta $(PILON_DIR)/%.pilon.changes : $(GENOME_DIR)/%.fa $(BWA_OUTDIR)/ss2_CGATGT_s_6.bam $(BWA_OUTDIR)/ss2_CGATGT_L004.bam $(BWA_OUTDIR)/SRR1010369.bam $(BWA_OUTDIR)/SRR998854.bam $(BAMS_INDICES)
	$(dir_guard)
	java -Djava.io.tmpdir=$(TEMP_DIR) -Xmx$(JAVA_MAXMEM)M -jar $(PILON_JAR_DIR)/pilon-1.16.jar --changes --genome $(word 1,$^) --frags $(word 2,$^) --frags $(word 3,$^) --jumps $(word 4,$^) --frags $(word 5,$^) --output $(@D)/$*.pilon

$(OUTPUT_GENOME) : $(PILON_FASTA)
	$(dir_guard)
	cp $< $@
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
clean_fastqs : 
	rm -rf $(RAW_FASTQ_DIR) $(FINAL_FASTQ_DIR)

clean :
	rm -rf $(TEMP_DIR)

clean_bams :
	rm -rf $(BWA_OUTDIR)

clean_qc :
	rm -rf $(QC_DIR)

pristine : clean_fastqs clean clean_bams clean_qc
	rm -rf $(GENOME_DIR) $(INFO_DIR) $(PILON_DIR)

clean_ncbi_cache :
	ls -ltr /home/josh/ncbi/public/sra
	cache-mgr --clear
	ls -ltr /home/josh/ncbi/public/sra
