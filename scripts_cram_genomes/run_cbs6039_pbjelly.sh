# /ssh:microbe:/home/josh/collabs/HeitmanLab/camy_centromere/scripts/run_cbs6039_pbjelly.sh

export PATH=$PATH:/opt/blasr/alignment/bin/
source /home/josh/collabs/software/PBSuite_14.1.15/setup.sh
# source /home/josh/collabs/software/Jelly_14.1.14/setup.sh
# source $CAMY/scripts/exportPaths.sh

which sawriter
which blasr
##==============================================================================
PROTOCOL=$CAMY/config/pbsuite/pbjelly_cbs6039.xml

BASE_DIR=/nfs/gems_sata/heitman/camy/reference_genome_contruction/CBS6039/pbjelly
DATA_DIR=$BASE_DIR/data
OUT_DIR=$BASE_DIR/out

REF_FILE=cbs6039_genome_v2.fasta
REF_ORIG=$CAMY/cram_genomes/${REF_FILE}
REF=$DATA_DIR/$REF_FILE

RAW_DIR=/nfs/gems_sata/heitman/camy/camy_raw_data
PACBIO_1=${DATA_DIR}/cbs6039_duke_pacbio.fastq
PACBIO_2=${DATA_DIR}/cbs6039_unc_pacbio.fastq
PACBIO_1_GZ=${RAW_DIR}/pacbio_duke/filtered_subreads_SS1.fastq.gz
PACBIO_2_GZ=${RAW_DIR}/pacbio_unc/022_SS001_10Kb_B01_0141_4_filtered_subreads.fastq.gz
##==============================================================================
rm -rf $DATA_DIR $OUT_DIR
mkdir -p $DATA_DIR $OUT_DIR

# set up links in BASE_DIR for protocol and this script - just for future reference
ln -s $PROTOCOL $BASE_DIR
ln -s $0 $BASE_DIR

# Link to reference sequence and gunzip fastqs to local dir
ln -s $REF_ORIG $REF
gzip -dc < $PACBIO_1_GZ > $PACBIO_1
gzip -dc < $PACBIO_2_GZ > $PACBIO_2
##==============================================================================

for STAGE in \
    setup \
    mapping \
    support \
    extraction \
    assembly \
    output
do 
    Jelly.py $STAGE $PROTOCOL
    sleep 2
done    

mv $OUT_DIR/jelly.out.fasta cbs6039_genome_v3.fasta
mv $OUT_DIR/jelly.out.qual cbs6039_genome_v3.qual
